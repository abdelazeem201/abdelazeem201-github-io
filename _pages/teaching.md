---
layout: archive
title: "Teaching"
permalink: /teaching/
author_profile: true
---
## ASIC Design Engineer
Teaching assistant for ASIC design internship program @ Zagazig University. My role is to help trainees with design and implementation procedures and grade their lab reports.

{% include base_path %}
> PDF version of my Lab calss can be found [here](/files/ASIC_Design.pdf).

