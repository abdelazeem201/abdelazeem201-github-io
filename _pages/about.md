---
permalink: /
title: "Welcome to my home page"
excerpt: "About me"
author_profile: true
header:
  teaser : teaser.png
redirect_from: 
  - /about/
  - /about.html
---

I am an extremely motivated Physical Design Engineer with interests in the field of Digital IC Design and Computer Architecture. I am regularly trusted to deliver on the toughest designs because I enjoy finding new approaches and I do not give up!

Besides the cutting-edge technology and tools I work with every day, I believe the best thing is the people! I enjoy learning from experienced Engineers. My objective is simple, work with my team to deliver continued success, and to keep learning Physical Design!, and I have done projects in both FPGA and ASIC, details can be found in the project section of this page or on my [GitHub](https://github.com/abdelazeem201).

My area of interest lies in ASIC design, Timing analysis(STA), Physical Design, and Logic Synthesis

- 🔭 I’m currently an FPGA Designer at [R&D Center "EAD Military"](http://www.mtc.edu.eg/mtcwebsite/ResearchCenter.aspx).

- 💬 Ask me about ASIC Design "Verilog, VHDL, STA, PnR"

- 💼 If you have questions, or you have a project I'm available to help and also I'm looking for a new opportunities, you can always contact me.

 

