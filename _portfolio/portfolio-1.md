---
title: "systolic array based Google TPU"
excerpt: "This project implements a systolic array, from behavior level (RTL) to tape-out.<br/><img src='/images/arch of Sys.png'>"
collection: portfolio
---

[Link](https://github.com/abdelazeem201/Systolic-array-implementation-in-RTL-for-TPU)
